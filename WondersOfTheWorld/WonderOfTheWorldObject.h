//
//  WonderOfTheWorldObject.h
//  WondersOfTheWorld
//
//  Created by Michael Childs on 2/29/16.
//  Copyright © 2016 Michael Childs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MapKit/Mapkit.h"

@interface WonderOfTheWorldObject : NSObject

@property (nonatomic, strong) NSString* image;
@property (nonatomic, strong) NSString* image_thumb;
@property (nonatomic, strong) NSString* location;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* region;
@property (nonatomic, strong) NSString* wikipedia;
@property (nonatomic, strong) NSString* year_built;

@property (nonatomic, assign) CLLocationCoordinate2D coord;

@end
