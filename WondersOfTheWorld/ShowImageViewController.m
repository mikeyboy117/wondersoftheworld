//
//  ShowImageViewController.m
//  WondersOfTheWorld
//
//  Created by Michael Childs on 2/29/16.
//  Copyright © 2016 Michael Childs. All rights reserved.
//

#import "ShowImageViewController.h"

@interface ShowImageViewController ()

@end

@implementation ShowImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.detailedImage = [[UIImageView alloc]initWithFrame:self.view.bounds];
    
    self.detailedImage.contentMode = UIViewContentModeScaleAspectFit;
    
    self.view.backgroundColor = [UIColor blackColor];
    
    [self.view addSubview:self.detailedImage];
    
    
    NSURL *url = [NSURL URLWithString:self.imgURL];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    [[session dataTaskWithRequest:request
                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        //Your main thread code goes in here
                        self.detailedImage.image = [UIImage imageWithData:data];
                    });
                    
                    
                    
                }] resume];
    
    UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageExit)];
    [self.view addGestureRecognizer:gesture];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)imageExit{
    [self dismissViewControllerAnimated:YES completion:NULL];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
