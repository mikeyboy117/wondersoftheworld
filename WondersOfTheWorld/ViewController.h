//
//  ViewController.h
//  WondersOfTheWorld
//
//  Created by Michael Childs on 2/29/16.
//  Copyright © 2016 Michael Childs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WonderOfTheWorldObject.h"
@interface ViewController : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
{
    NSMutableArray* arrayOfSevenWonders;
    
    UICollectionView* cView;
}

@property (weak, nonatomic) IBOutlet UIImageView *imageview;

@end

