//
//  ShowImageViewController.h
//  WondersOfTheWorld
//
//  Created by Michael Childs on 2/29/16.
//  Copyright © 2016 Michael Childs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowImageViewController : UIViewController

@property (nonatomic, strong) UIImageView* detailedImage;
@property (nonatomic, strong) NSString* imgURL;

@end
