//
//  ViewController.m
//  WondersOfTheWorld
//
//  Created by Michael Childs on 2/29/16.
//  Copyright © 2016 Michael Childs. All rights reserved.
//

#import "ViewController.h"
#import "WonderOfTheWorldObject.h"
#import "ShowImageViewController.h"

@interface ViewController ()

@end

@implementation ViewController

typedef void(^myCompletion)(NSArray* data);

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    cView=[[UICollectionView alloc] initWithFrame:self.view.frame collectionViewLayout:layout];
    [cView setDataSource:self];
    [cView setDelegate:self];
    
    [cView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [cView setBackgroundColor:[UIColor colorWithRed:0.243 green:0.278 blue:0.310 alpha:1.00]];
    
    [self.view addSubview:cView];
    
    
    arrayOfSevenWonders = [NSMutableArray new];
    
    [self getSevenWondersInfoWithResponseBlock:^(NSArray *array) {
        
        for (NSDictionary* d in array) {
            WonderOfTheWorldObject* wotwo = [WonderOfTheWorldObject new];
            wotwo.image = d[@"image"];
            wotwo.image_thumb = d[@"image_thumb"];
            wotwo.name = d[@"name"];
            
            [arrayOfSevenWonders addObject:wotwo];
            
            
//            NSURL *url = [NSURL URLWithString:wotwo.image_thumb];
//            
//            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
//            
//            
//            NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
//            
//            [[session dataTaskWithRequest:request
//                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//                            
//                            dispatch_async(dispatch_get_main_queue(), ^{
//                                //Your main thread code goes in here
//                                self.imageview.image = [UIImage imageWithData:data];
//                            });
//                            
//                            
//                            
//                        }] resume];
        }
        
        
        
        
    }];
    
    
}

-(void)getSevenWondersInfoWithResponseBlock:(myCompletion) completion {
    NSString *urlString = [NSString stringWithFormat: @"http://aasquaredapps.com/Class/sevenwonders.json"];
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    [[session dataTaskWithRequest:request
                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    
                    // do stuff
                    completion([NSJSONSerialization JSONObjectWithData:data options:0 error:&error]);
                    
                }] resume];
}

#pragma collectionView stuff
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrayOfSevenWonders.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    cell.backgroundColor=[UIColor colorWithRed:0.537 green:0.886 blue:0.867 alpha:1.00];
    
    UIImageView* iv = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height)];
    
    UILabel* lbl = [[UILabel alloc]initWithFrame:CGRectMake(cell.frame.size.width/4, cell.frame.size.height-50, cell.frame.size.width, 50)];
    
    lbl.textColor = [UIColor whiteColor];
    
    WonderOfTheWorldObject* wotwo = arrayOfSevenWonders[indexPath.row];
    
    NSURL *url = [NSURL URLWithString:wotwo.image_thumb];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    [[session dataTaskWithRequest:request
                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        //Your main thread code goes in here
                        iv.image = [UIImage imageWithData:data];
                    });
                    
                    
                    
                }] resume];
    
    

    [cell addSubview:iv];
    
    lbl.text = wotwo.name;
    
    [cell addSubview:lbl];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.view.bounds.size.width/2 - 5, 250);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"tap");
    
    ShowImageViewController* sivc = [ShowImageViewController new];
    WonderOfTheWorldObject* wotwo = arrayOfSevenWonders[indexPath.row];
    
    sivc.imgURL = wotwo.image;
    
    [self presentViewController:sivc animated:YES completion:NULL];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
